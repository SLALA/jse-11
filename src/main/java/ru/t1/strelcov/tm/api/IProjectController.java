package ru.t1.strelcov.tm.api;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void findById();

    void findByName();

    void findByIndex();

    void removeById();

    void removeByName();

    void removeByIndex();

    void updateById();

    void updateByName();

    void updateByIndex();

}
