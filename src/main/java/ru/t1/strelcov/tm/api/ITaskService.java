package ru.t1.strelcov.tm.api;

import ru.t1.strelcov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task add(String name, String description);

    void add(Task task);

    void clear();

    void remove(Task task);

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByName(String oldName, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
